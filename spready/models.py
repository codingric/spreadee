from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func
from database import Base
from flask_login import UserMixin
import datetime
from dateutil.tz import tzlocal

class Player(Base):
  __tablename__ = 'players'
  id = Column(Integer, primary_key=True)
  joined = Column(DateTime(), server_default=func.now())
  user_id = Column(Integer, ForeignKey('users.id'))
  game_id = Column(Integer, ForeignKey('games.id'))

  def __init__(self, game_id, user_id):
    self.game_id = game_id
    self.user_id = user_id
    self.joined = datetime.datetime.now(tzlocal())

class User(Base, UserMixin):
  __tablename__ = 'users'
  id = Column(Integer, primary_key=True)
  created = Column(DateTime(), server_default=func.now())
  name = Column(String)
  email = Column(String)
  avatar = Column(String)
  token = Column(String)

  particited = relationship('Game', secondary='players', backref='participants')
  
  def __init__(self, token, name, email, avatar):
    self.name = name
    self.email = email
    self.avatar = avatar
    self.token = token

  def get_id(self):
    return self.id

class Game(Base):
  __tablename__ = 'games'
  id = Column(Integer, primary_key=True)
  created = Column(DateTime(), server_default=func.now())
  started = Column(DateTime())
  finished = Column(DateTime())
  creator_id = Column(Integer, ForeignKey('users.id'))
  winner_id = Column(Integer, ForeignKey('users.id'))
  tournament_id = Column(Integer, ForeignKey('tournaments.id'))

  creator = relationship('User', foreign_keys=creator_id, backref='initiated')
  winner = relationship('User', foreign_keys=winner_id, backref='wins')

  def __init__(self, tournament_id, creator_id):
    self.tournament_id = tournament_id
    self.creator_id = creator_id
    self.created = datetime.datetime.now(tzlocal())

class Tournament(Base):
  __tablename__ = 'tournaments'
  id = Column(Integer, primary_key=True)
  created = Column(DateTime(), server_default=func.now())
  name = Column(String(64))
  game_type = Column(String(64))
  games = relationship('Game', backref='tournament')
