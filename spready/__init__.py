from flask import Flask, render_template, session, flash
from flask import abort, redirect, url_for, request
from flask_login import LoginManager, login_required, current_user, login_user, logout_user
from flask_googlelogin import GoogleLogin
from requests_oauthlib import OAuth2Session
from database import db_session
from models import *
from datetime import datetime
from sqlalchemy import or_, and_
from sqlalchemy import func
import re, time, datetime, uuid
import httplib2
from dateutil.tz import tzlocal

from oauth2client.client import OAuth2WebServerFlow
from apiclient.discovery import build

from wtforms import Form, BooleanField, StringField, PasswordField, validators, SelectField

app = Flask(__name__)
app.secret_key = '990330172148-gq458bs715gl2gr1trsdrthqpkfaieub'
app.debug = True

GOOGLE_CLIENT_ID = '990330172148-gq458bs715gl2gr1trsdrthqpkfaieub.apps.googleusercontent.com'
GOOGLE_CLIENT_SECRET = 'nAwPt6Iz49FfIP1Ly71uMBhN'

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

@app.before_request
def make_session_permanent():
    session.permanent = True

@app.route('/')
@login_required
def index():
  games = Game.query.filter(Game.finished == None).all()
  finished = Game.query.filter(Game.finished != None).order_by(Game.id.desc()).limit(3).all()
  return render_template('games.html', games=games, finished=finished)


@app.route('/stats')
@app.route('/stats/<int:user_id>')
@login_required
def stats(user_id=None):
  selected_user = User.query.get(user_id) if user_id else current_user
  played = Game.query.join(Player, Player.game_id == Game.id).filter(
    and_(
      Player.user_id == selected_user.id,
      Game.finished != None
    )
  ).all()
  won = Game.query.filter(
    and_(
      Game.winner_id == selected_user.id,
      Game.finished != None
    )
  ).all()

  foes_sql = """
      with results(game_id, result, duration) as (
        select                 
          g.id game_id
          , (case when g.winner_id = {me} then 'win' else 'loss' end) result
          , (strftime('%s', g.finished) - strftime('%s', g.created))/60 duration
        from games g           
        join players p on p.game_id = g.id
        where g.winner_id      
        group by g.id          
        having count(case when p.user_id = {me} then 1 end) 
      )
      select 
        u.name                 
        , count(g.id) total
        , count(case when r.result = 'win' then 1 end) wins
        , count(case when g.winner_id = p.user_id then 1 end) loss
        , avg(r.duration) duration
      from players p           
      join results r on r.game_id = p.game_id
      join users u on p.user_id = u.id
      join games g on g.id = p.game_id
      where u.id != {me}
      group by u.name          
      order by 2 desc, 3 desc
      limit 10;""".format(me=selected_user.id)

  foes = db_session.execute(foes_sql).fetchall()

  results_sql = """
      with results(game_id, result) as (
        select
          g.id game_id
          , (case when g.winner_id = {me} then 'win' else 'loss' end) result
        from games g
        join players p on p.game_id = g.id
        where g.winner_id
        group by g.id
        having count(case when p.user_id = {me} then 1 end)
      )
      select 
        (select count(*) from results r where r.game_id < rr.game_id)+1 rowid
        , rr.* 
      from results rr 
      order by 1 desc limit 10
  """.format(me=selected_user.id)

  results = db_session.execute(results_sql).fetchall()

  streak_sql = """
    select 
      x.result, 
      count(*) streak 
    from (
      select
        g.id
        , (case when g.winner_id = {me} then 'win' else 'loss' end) result
        , ifnull(max(g2.id), 0) streak_id
      from games g
      join players p on p.game_id = g.id
      left join (
        select
          g.id
          , (case when g.winner_id = {me} then 'win' else 'loss' end) result
        from games g
        join players p on p.game_id = g.id
        where g.winner_id
        group by g.id
        having count(case when p.user_id = {me} then 1 end)) g2
          on g2.id < g.id and g2.result != (case when g.winner_id = {me} then 'win' else 'loss' end)
      where g.winner_id
      group by g.id
      having count(case when p.user_id = {me} then 1 end)
    ) x 
    group by x.streak_id 
    order by x.streak_id desc
  """.format(me=selected_user.id)
  streak = db_session.execute(streak_sql).fetchall()

  ladder = db_session.execute("""
    select 
      u.id,
      u.name, 
      count(case when g.winner_id = u.id then 1 end) wins,
      count(case when g.winner_id != u.id then 1 end) losses,
      count(case when g.winner_id = u.id then 1 end)*100/count(*) ratio,
      ifnull(count(case when g.winner_id = u.id then 1 end)*1.0/count(case when g.winner_id != u.id then 1 end), 1) * count(*) rating
    from users u 
    join players p 
      on u.id = p.user_id
    join games g 
      on g.id = p.game_id 
    where g.finished is not null group by u.id
    order by rating desc limit 10;""").fetchall()

  if selected_user.id != current_user.id:
    versus = db_session.execute("""
      select
        count(case when g.winner_id = {me} then 1 end) as wins,
        count(case when g.winner_id not in ({me},{them}) then 1 end) as noone,
        count(case when g.winner_id = {them} then 1 end) as losses
      from games g
      join (select p.game_id from players p where p.user_id in ({me},{them}) group by p.game_id having count(*) = 2) vg
        on vg.game_id = g.id""".format(me=current_user.id, them=selected_user.id)).fetchall()
  else:
    versus = None

  timesplit = db_session.execute("""
    select 
      (strftime('%s', finished)-strftime('%s', created))/60 as taken, 
      count(*) as n 
    from games g 
    join players p 
      on p.game_id = g.id 
    where p.user_id = {}
    group by (strftime('%s', finished)-strftime('%s', created))/60""".format(selected_user.id)).fetchall()
  return render_template('stats.html', won=won, played=played, ladder=ladder, timesplit=timesplit, selected_user=selected_user, versus=versus, streak=streak, results=results, foes=foes)

@app.route('/tournaments')
@login_required
def tournaments():
  return render_template('tournaments.html', tournaments=Tournament.query.all())

class NewGameForm(Form):
  tournament_id = SelectField(u'Tournament', coerce=int)

@app.route('/game', methods=['GET', 'POST'])
@login_required
def newgame():
  form = NewGameForm(request.form)
  form.tournament_id.choices = [ (t.id, t.name) for t in Tournament.query.order_by('name') ]
  if request.method == 'POST' and form.validate():
    g = Game(form.tournament_id.data, current_user.get_id())
    db_session.add(g)
    db_session.commit()
    p = Player(g.id, current_user.get_id())
    db_session.add(p)
    db_session.commit()
    flash("Ready to roll!", "fyi")
    return redirect(url_for('index'))

  return render_template('new-game.html', form=form)

@app.route('/game/<int:game_id>', methods=['POST'])
@login_required
def game(game_id):
  g = Game.query.get(game_id)

  if request.form.get('action') == 'join':
    #todo check that player not already in game
    p = Player(game_id, current_user.get_id())
    db_session.add(p)
    db_session.commit()
    flash("You're in!", "fyi")
    return redirect(url_for('index'))

  if request.form.get('action') == 'leave':
    if g.creator == current_user and len(g.participants) > 1:
      flash("You have to wait until everyone else leaves","error")
      return redirect(url_for('index'))
    p = Player.query.filter(
        and_(
          Player.user_id == current_user.id,
          Player.game_id == game_id
        )).first()
    if ((datetime.datetime.now() - p.joined).seconds / 60) >= 2:
      flash("Sorry you can only leave after 2 mins", "error")
      return redirect(url_for('index'))
    db_session.delete(p)
    if len(g.participants) == 1:
      db_session.delete(g)
      flash("Game removed", "fyi")
    db_session.commit()
    return redirect(url_for('index'))

  if request.form.get('winner_id'):
    w = User.query.get(request.form.get('winner_id'))
    g.winner_id = w.id
    g.finished = datetime.datetime.now()
    db_session.add(g)
    db_session.commit()
    flash("Better lucky next time", "fyi")
    return redirect(url_for('index'))

  flash("Not sure what you are trying to get at", "error")
  return redirect(url_for('index'))

@app.template_filter('th')
def th(n):
  suffix = ['th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th']
  if n < 0:
    n *= -1
  n = int(n)
  if n % 100 in (11,12,13):
    s = 'th'
  else:
    s = suffix[n % 10]
  return str(n) + s

@app.template_filter('duration')
def duration(then):
  if not then:
    return "Never"
  start = time.mktime(then.timetuple())
  end = time.mktime(datetime.datetime.now().timetuple())
  hours, rem = divmod(end-start, 3600)
  minutes, seconds = divmod(rem, 60)

  
  days, hours = divmod(hours, 24)
  months, days = divmod(days, 30)
  weeks, days = divmod(days, 7)

  parts = []
  if months > 1:
    parts.append("%d months" % months)
  elif months == 1:
    parts.append("1 month")

  if weeks > 1:
    parts.append("%d weeks" % weeks)
  elif weeks == 1:
    parts.append("1 week")

  if days > 1:
    parts.append("%d days" % days)
  elif days == 1:
    parts.append("1 day")

  if hours > 1:
    parts.append("%d hrs" % hours)
  elif hours == 1:
    parts.append("1 hr")

  if minutes > 1:
    parts.append("%d mins" % minutes)
  elif minutes == 1:
    parts.append("1 min")

  if len(parts) < 1:
    return 'Just now'

  return "%s ago" % parts[0]


@app.route('/login')
def login():
  if current_user.is_authenticated():
    return redirect(url_for('index'))


  params = {'code': request.args.get('code'), 'callback': request.args.get('callback')}

  flow = OAuth2WebServerFlow(
    client_id=GOOGLE_CLIENT_ID,
    client_secret=GOOGLE_CLIENT_SECRET,
    scope='profile email')

  if params.get('code') is None:
    if request.args.get('google') is None:
      return render_template('login.html')
    return redirect(flow.step1_get_authorize_url(request.base_url))

  flow.redirect_uri = request.base_url

  credentials = flow.step2_exchange(params.get('code'))
  http = httplib2.Http()
  cred_http = credentials.authorize(http)
  p = build("oauth2", "v1", http=cred_http)
  userinfo = p.userinfo().get().execute(cred_http)

  u = User.query.filter(User.token==userinfo.get('id')).first()
  if not u:
    u = User(
      userinfo.get('id'),
      userinfo.get('name'),
      userinfo.get('email'),
      userinfo.get('picture')
    )
    db_session.add(u)
    db_session.commit()

  login_user(u, force=True)
  flash("Welcome back!", "fyi")
  return redirect(url_for('index'))

@app.route('/logout')
@login_required
def logout():
  logout_user()
  return redirect(url_for('login'))
