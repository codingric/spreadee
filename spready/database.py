from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

import datetime

engine = create_engine('sqlite:///spready.db', convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()

def init_db():
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()
    import models
    Base.metadata.create_all(bind=engine)

def init_data():
  from models import *
  t = Tournament()
  t.name = 'Lunchtime monops'
  t.game_type = 'Monopoly'

  g = Game()
  g.started = datetime.datetime.now()

  t.games.append(g)

  ric = User()
  ric.name = 'Ricardo'

  james = User()
  james.name = 'James'

  g.participants.append(ric)
  g.participants.append(james)
  g.creator = ric
  db_session.add(g)
  db_session.add(t)
  db_session.commit()
